use std::{env, io};
use std::collections::HashMap;
use futures_util::{pin_mut, TryStreamExt};
use rspotify::{AuthCodeSpotify, Credentials, OAuth, scopes};
use rspotify::prelude::*;

#[derive(Copy, Clone, Debug)]
pub enum AppError {
    ParseError,
}

#[tokio::main]
async fn main() -> Result<(), AppError> {
    println!("Client ID: ");
    let mut id = String::new();
    io::stdin().read_line(&mut id).expect("Could not read input");
    id = id.trim_end().to_string();

    println!();

    println!("Client Secret: ");
    let mut secret = String::new();
    io::stdin().read_line(&mut secret).expect("Could not read input");
    secret =  secret.trim_end().to_string();

    println!();

    println!("Client ID is {id}");
    println!("Client Secret is {secret}");

    println!();

    let spotify = get_spotify(Credentials::new(id.as_str(), secret.as_str())).await;

    let url = spotify.get_authorize_url(false).unwrap();

    spotify.prompt_for_token(&url).await.unwrap();

    let saved_tracks = spotify.current_user_saved_tracks(None);

    pin_mut!(saved_tracks);

    let mut hashmap: HashMap<String, i64> = HashMap::new();

    let mut num_songs = 0;

    while let Some(track) = saved_tracks.try_next().await.unwrap() {
        let artist = hashmap.entry(track.track.artists[0].clone().name).or_insert(0);
        *artist += 1;
        num_songs += 1;
    }

    let mut vec: Vec<_> = hashmap.iter().collect();

    vec.sort_by(|a, b| {
        a.1.partial_cmp(b.1).unwrap()
    });

    vec.reverse();

    let mut pos = 1;

    for item in vec {
        println!("{} '{}': {} ({:.3}%)", pos, item.0, item.1, (item.1.clone() as f64 / num_songs as f64) * 100.0);
        pos += 1;
    }

    Ok(())
}

pub async fn get_spotify(creds: Credentials) -> AuthCodeSpotify {

    let scopes = scopes!(
        "user-library-read",
        "playlist-read-collaborative",
        "playlist-read-private"
    );

    env::set_var("RSPOTIFY_REDIRECT_URI", "http://localhost:8888/callback");

    let oauth = OAuth::from_env(scopes).unwrap();

    let spotify = AuthCodeSpotify::new(creds, oauth);

    return spotify;
}